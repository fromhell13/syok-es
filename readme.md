# SYOK Elasticsearch Export

## Description
- This worker will export all content from SYOK RDS into Elasticsearch. The query will look into published contents and re-index index in Elasticsearch.
- Run this worker if you want to refresh search content or export missing content.

## Elastic Beanstalk environment
SyokElasticsearch-Prod

## Setup
- Setup *eb cli* for easier deployment to Elastic Beanstalk
- Clone this repo
```
git clone
```
- Install dependency
```
npm install
```

## Deploy
- Please make sure to use a correct host */src/config/index.js* before deploy
- run eb cli command to deploy
```
eb deploy
```
