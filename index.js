require('module-alias/register')
const express = require('express')
const app = express()
const morgan = require('morgan')
const {type} = require('@config')

const ExportArticles        = require('./src/articles')
const ExportVideos          = require('./src/video')
const ExportContest         = require('./src/contests')
const ExportPodcastEpisode  = require('./src/podcast')
const ExportContestVotes    = require('./src/contestvote')

const articles     = new ExportArticles()
const video        = new ExportVideos()
const contest      = new ExportContest()
const podcast      = new ExportPodcastEpisode()
const contestvote  = new ExportContestVotes()

const moment = require('moment')


app.use(morgan('combined'));

app.get('/', async (req, res) => {
    res.send("Hello world")
})

app.get('/start', async (req,res) =>{
    try{
        await contest.startExport()
        await contestvote.startExport()
        await podcast.startExport()
        await video.startExport()
        await articles.startExport()
        res.send('Elastic Search Ingestion Completed')
    }catch(err){
        console.log('Error occured: '. err)
    }
    
})

app.get('/date', async (req,res) => {
    // === Set Date to +800 ===
    const dt = moment().add(8, 'hours').format()
    res.send(dt)
})



// starting the server
 const server = app.listen(3001, () => {
    const port = server.address().port
    console.log("cron Ingestion app listening at port", port)
});






