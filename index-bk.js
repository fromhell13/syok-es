require('module-alias/register')

const ExportArticles = require('./src/articles')
const ExportVideos   = require('./src/video')
const ExportContest  = require('./src/contests')
const articles = new ExportArticles()
const video    = new ExportVideos()
const contest  = new ExportContest()

articles.startExport();
video.startExport();
contest.startExport();


