module.exports = {
    batch:100,
    mysql: {
        database: process.env.database,
        host: process.env.host,
        username: process.env.username,
        password: process.env.password
    },
    elasticsearch: {
        forceMigrate: true,
        host: 'xxxxxxxxxxx',
        cloudId: '',
        username: '',
        password: ''
        
    },
    indices:{
        syok_articles: {
            index: 'syok_articles',
            settings: {
                number_of_shards: 3,
                number_of_replicas: 2
            },
            mappings: {
                properties: {
                    id: { type: 'integer' },
                    Name: { type: 'text' },
                    Description: { type: 'text' },
                    Tag: { type: 'text' },
                    LandscapeImage: { type: 'text' },
                    DocumentPublishFrom: { type: 'date' },
                    DocumentPublishTo: { type: 'date' },
                    Language: { type: 'text' },
                    SiteName: { type: 'text' },
                    SiteLogo: { type: 'text' },
                    Link: { type: 'text' },
                    MobileLink: { type: 'text' },
                    type: { type: 'text' }
                }
            }
        }
    },
    language:{
        malay: 1,
        english: 2,
        chinese: 3,
        tamil: 4,
        mandarin: 3
    },
    type:{
        'article': 1,
        'video': 2,
        'Contest': 3,
        'episode': 4,
        'Syok Votes': 5
    }
}