const Sequelize = require('sequelize')
const config = require('@config')
const MySQLQuery = require('./query')


class MYSQLHelper {
    constructor(exportType){
        this.sequelize = new Sequelize(config.mysql.database, config.mysql.username, config.mysql.password, {
            dialect: 'mysql',
            host: config.mysql.host
        });
        this.exportType = exportType
        this.mysqlquery = new MySQLQuery()
    }

    async connect() {
        return this.sequelize.authenticate()

    }

    async close(){
        return this.sequelize.close()
    }
    // Return total count
    async getTotalCount() {
        //return this.sequelize.query(getAllArticleQuery)
        return this.sequelize.query(await this.mysqlquery.returnAll(this.exportType))
          .then(([results, metadata]) => {
            
            return results.length
        })
    }
    // Return offset and limit
    async returnsOffset(offset, limit) {
        return this.sequelize.query(await this.mysqlquery.returnOffset(offset, limit,this.exportType))
          .then(([results, metadata]) => {
            return results
          })
    }
}

module.exports = MYSQLHelper