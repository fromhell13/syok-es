const { elasticsearch, language, type} = require('@config')
const { Client } = require('@elastic/elasticsearch');
const moment = require('moment')

class ElasticSearchClient{
    constructor(esIndex){
        this.esIndex = esIndex;
    }

    async connect() {
        /*this.client = new Client({
            cloud: {
                id: elasticsearch.cloudId,
                username: elasticsearch.username,
                password: elasticsearch.password
            }
        });
        */
        this.client = new Client({
            node: {
                url: new URL(elasticsearch.host)
            }    
        })

        if (elasticsearch.forceMigrate) {
            await this.deleteIndex(this.esIndex);
        }
        await this.checkIndexExistOrCreate(this.esIndex);
        return true;
    }

    async deleteIndex(index) {
        try {
            await this.client.indices.delete({
                index
            });
            return true
        } catch(err) {
            console.log('Failed to delete index');
            return true
        }
    }

    async checkIndexExistOrCreate(index) {
        try {
            const indexExist = await this.client.indices.exists([{
                index
            }]);
            if (!indexExist) {
                console.log('Index not available');
                return this.createIndex(index);
            }
            return true
        } catch (err) {
            console.log('Issue while finding index', index);
            return this.createIndex(index);
        }
    }

    async createIndex(index) {
        try {
            await this.client.indices.create({
                //index,
                index: index
            });
            return true
        } catch(err) {
            console.log('Failed to create index', err);
            throw new Error('INDEX_CREATION_FAILED')
        }
    }

    async putArticles(results) {
        try {
            const body = [];
            results.forEach(item => {
                
                body.push({ index:  { _index: this.esIndex, _type: 'doc', _id: item.ID } });
                body.push({ 
                    id: item.ID, 
                    Title: item.Name, 
                    Image: item.LandscapeImage,
                    DocumentPublishFrom: (item.DocumentPublishFrom instanceof Date && !isNaN(item.DocumentPublishFrom.valueOf()))? moment(item.DocumentPublishFrom).subtract(8, 'hours').format(): null,
                    DocumentPublishTo: (item.DocumentPublishTo instanceof Date && !isNaN(item.DocumentPublishTo.valueOf()))? moment(item.DocumentPublishTo).subtract(8, 'hours').format(): null,
                    Language: language[item.Language.toLowerCase()],
                    PublisherName: item.SiteName,
                    SiteLogo: item.SiteLogo,
                    SharingLink: item.Link,
                    Link: item.MobileLink,
                    Duration: null,
                    Ratio: null,
                    type: "1"  // article type = 1
                });
            }) 
            const response = await this.client.bulk({
                body
            });
            //console.log('Record added to es', response);
            return true;
        } catch(err) {
            console.log('ES_INSERTION_FAILED', err);
        }
    }

    async putVideos(results) {
        try {
            const body = [];
            results.forEach(item => {
                
                body.push({ index:  { _index: this.esIndex, _type: 'doc', _id: item.ID } });
                body.push({ 
                    id: item.ID, 
                    Title: item.Name, 
                    Image: item.LandscapeImage,
                    DocumentPublishFrom: (item.DocumentPublishFrom instanceof Date && !isNaN(item.DocumentPublishFrom.valueOf()))? moment(item.DocumentPublishFrom).subtract(8, 'hours').format(): null,
                    DocumentPublishTo: (item.DocumentPublishTo instanceof Date && !isNaN(item.DocumentPublishTo.valueOf()))? moment(item.DocumentPublishTo).subtract(8, 'hours').format(): null,
                    Language: language[item.Language.toLowerCase()],
                    PublisherName: item.SiteName,
                    SiteLogo: item.SiteLogo,
                    SharingLink: item.Link,
                    Link: item.M3U8Link,
                    Duration: null,
                    Ratio: item.ratio,
                    type: "2"  // video type = 2
                });
            })
            const response = await this.client.bulk({
                body
            });
            //console.log('Record added to es', response);
            return true;
        } catch(err) {
            console.log('ES_INSERTION_FAILED', err);
        }
    }

    async putContests(results) {
        try {
            const body = [];
            results.forEach(item => {
                
                body.push({ index:  { _index: this.esIndex, _type: 'doc', _id: item.ID } });
                body.push({ 
                    id: item.ID, 
                    Title: item.Name, 
                    Image: item.LandscapeImage,
                    DocumentPublishFrom: (item.DocumentPublishFrom instanceof Date && !isNaN(item.DocumentPublishFrom.valueOf()))? moment(item.DocumentPublishFrom).subtract(8, 'hours').format(): null,
                    DocumentPublishTo: (item.DocumentPublishTo instanceof Date && !isNaN(item.DocumentPublishTo.valueOf()))? moment(item.DocumentPublishTo).subtract(8, 'hours').format(): null,
                    Language: language[item.Language.toLowerCase()],
                    PublisherName: item.SiteName,
                    SiteLogo: item.SiteLogo,
                    SharingLink: item.Link,
                    Link: item.MobileLink,
                    Duration: null,
                    Ratio: null,
                    type: "3"  // contest type = 3
                });
            })
            const response = await this.client.bulk({
                body
            });
            //console.log('Record added to es', response);
            return true;
        } catch(err) {
            console.log('ES_INSERTION_FAILED', err);
        }
    }

    async putPodcastEpisode(results) {
        try {
            const body = [];
            results.forEach(item => {
                
                body.push({ index:  { _index: this.esIndex, _type: 'doc', _id: item.EpisodeId } });
                body.push({ 
                    id: item.EpisodeId, 
                    Title: item.Title,
                    Playlist: item.PlaylistTitle,
                    Image: item.Image,
                    DocumentPublishFrom: (item.PublishedAt instanceof Date && !isNaN(item.PublishedAt.valueOf()))? moment(item.PublishedAt).subtract(8, 'hours').format(): null,
                    DocumentPublishTo: null,
                    Language: language[item.Language.toLowerCase()],
                    PublisherName: item.PublisherName,
                    SiteLogo: null,
                    SharingLink: null,
                    Link: item.Source,
                    Duration: item.Duration,
                    Ratio: null,
                    type: "4"  // podcast episode type = 4
                }); 
            })
            const response = await this.client.bulk({
                body
            });
            //console.log('Record added to es', response);
            return true;
        } catch(err) {
            console.log('ES_INSERTION_FAILED', err);
        }
    }

    async putContestsVotes(results) {
        try {
            const body = [];
            results.forEach(item => {
                
                body.push({ index:  { _index: this.esIndex, _type: 'doc', _id: item.ID } });
                body.push({ 
                    id: item.ID, 
                    Title: item.Name, 
                    Image: item.LandscapeImage,
                    DocumentPublishFrom: (item.DocumentPublishFrom instanceof Date && !isNaN(item.DocumentPublishFrom.valueOf()))? moment(item.DocumentPublishFrom).subtract(8, 'hours').format(): null,
                    DocumentPublishTo: (item.DocumentPublishTo instanceof Date && !isNaN(item.DocumentPublishTo.valueOf()))? moment(item.DocumentPublishTo).subtract(8, 'hours').format(): null,
                    Language: language[item.Language.toLowerCase()],
                    PublisherName: item.SiteName,
                    SiteLogo: item.SiteLogo,
                    SharingLink: item.Link,
                    Link: item.MobileLink,
                    Duration: null,
                    Ratio: null,
                    type: type[item.Type]
                });
            })
            const response = await this.client.bulk({
                body
            });
            console.log('Record added to es', response);
            return true;
        } catch(err) {
            console.log('ES_INSERTION_FAILED', err);
        }
    }

}

module.exports = ElasticSearchClient;