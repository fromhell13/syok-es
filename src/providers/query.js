const moment = require('moment')

// === Set Date to +800 ===
//const dt = moment().add(8, 'hours').format()

class MySQLQuery{
    constructor(){
        
    }

    async returnAll(exportType){

        if(exportType == 'syok_articles'){ // *** Article ***
            return `SELECT * from syokcontentV2 WHERE type='article' \
            AND DocumentPublishFrom <='${moment().add(8, 'hours').format()}' \
            AND (DocumentPublishTo IS NULL OR DocumentPublishTo = '0000-00-00 00:00:00' \
            OR DocumentPublishTo >= '${moment().add(8, 'hours').format()}')`;
        } else if(exportType == 'syok_videos'){ // *** Video ***
            return `SELECT * from syokcontentV2 WHERE type='video' \
            AND DocumentPublishFrom <='${moment().add(8, 'hours').format()}' \
            AND (DocumentPublishTo IS NULL OR DocumentPublishTo = '0000-00-00 00:00:00' \
            OR DocumentPublishTo >= '${moment().add(8, 'hours').format()}')`;
        } else if(exportType == 'syok_contests'){ // *** Contest ***
            return `SELECT * from syokcontentV2 WHERE type='contest' \
            AND DocumentPublishFrom <='${moment().add(8, 'hours').format()}' \
            AND (DocumentPublishTo IS NULL OR DocumentPublishTo = '0000-00-00 00:00:00' \
            OR DocumentPublishTo >= '${moment().add(8, 'hours').format()}')`;
        } else if(exportType == 'syok_podcast_episodes'){ // *** Podcast Episodes***
            return `SELECT e.EpisodeId, e.Title, e.Image, e.PublishedAt, e.Duration, \
            e.Source, p.Language, p.PublisherName from episodes e, playlists p \
            WHERE e.PlaylistId = p.WhooshkaId \
            AND e.PublishedAt <='${moment().add(8, 'hours').format()}'`;
        } else if(exportType == 'syok_vote_contests'){ // *** Contests & Votes ***
            return `SELECT * from syokcontentV2 WHERE type IN ('contest', 'syok votes') \
            AND DocumentPublishFrom <='${moment().add(8, 'hours').format()}' \
            AND (DocumentPublishTo IS NULL OR DocumentPublishTo = '0000-00-00 00:00:00' \
            OR DocumentPublishTo >= '${moment().add(8, 'hours').format()}')`;
        }
        
    }

    async returnOffset(offset,limit,exportType){
        if(exportType == 'syok_articles'){ // *** article ***
            return `SELECT * from syokcontentV2 WHERE type='article' \
            AND DocumentPublishFrom <='${moment().add(8, 'hours').format()}' \
            AND (DocumentPublishTo IS NULL OR DocumentPublishTo = '0000-00-00 00:00:00' \
            OR DocumentPublishTo >= '${moment().add(8, 'hours').format()}') \
            LIMIT ${limit} OFFSET ${offset}`;
        } else if(exportType == 'syok_videos') { // *** video ***
            return `SELECT * from syokcontentV2 WHERE type='video' \
            AND DocumentPublishFrom <='${moment().add(8, 'hours').format()}' \
            AND (DocumentPublishTo IS NULL OR DocumentPublishTo = '0000-00-00 00:00:00' \
            OR DocumentPublishTo >= '${moment().add(8, 'hours').format()}') \
            LIMIT ${limit} OFFSET ${offset}`;
        } else if(exportType == 'syok_contests') { // *** contest ***
            return `SELECT * from syokcontentV2 WHERE type='contest' \
            AND DocumentPublishFrom <='${moment().add(8, 'hours').format()}' \
            AND (DocumentPublishTo IS NULL OR DocumentPublishTo = '0000-00-00 00:00:00' \
            OR DocumentPublishTo >= '${moment().add(8, 'hours').format()}') \
            LIMIT ${limit} OFFSET ${offset}`;
        } else if(exportType == 'syok_podcast_episodes'){ // *** Podcast Episodes***
            return `SELECT e.EpisodeId, e.Title, e.Image, e.PublishedAt, e.Duration, \
            e.Source, p.Language, p.PublisherName, p.Title as PlaylistTitle from episodes e, playlists p \
            WHERE e.PlaylistId = p.WhooshkaId \
            AND e.PublishedAt <='${moment().add(8, 'hours').format()}' \
            LIMIT ${limit} OFFSET ${offset}`;
        } else if(exportType == 'syok_vote_contests') { // *** Contests & Votes ***
            return `SELECT * from syokcontentV2 WHERE type IN ('contest', 'syok votes') \
            AND DocumentPublishFrom <='${moment().add(8, 'hours').format()}' \
            AND (DocumentPublishTo IS NULL OR DocumentPublishTo = '0000-00-00 00:00:00' \
            OR DocumentPublishTo >= '${moment().add(8, 'hours').format()}') \
            LIMIT ${limit} OFFSET ${offset}`;
        }
        
    }
}

module.exports = MySQLQuery