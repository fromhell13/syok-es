const config = require('@config')
const MYSQLHelper = require('../providers/mysql')
const ElasticSearchClient = require('../providers/elasticsearch')

const exportType = 'syok_vote_contests';

class ExportContestVotes {
    constructor(){
        this.mysql = new MYSQLHelper(exportType)
        this.esClient = new ElasticSearchClient(exportType);
    }

    async connect(){
        await this.mysql.connect()
        await this.esClient.connect()
        console.log('Contest & Votes Connection Established');
    }

    async close(){
        await this.mysql.close()
    }

    async getTotalCount(){
        return await this.mysql.getTotalCount()
    }

    async returnOffset(offset,limit){
        return await this.mysql.returnsOffset(offset,limit)
    }

    async batchProcess(totalCount){
        const limit = config.batch
        const batches = Math.floor(totalCount / limit) + 1
        console.log(`Total Contests & Votes to be imported : ${totalCount}, Batches Created : ${batches}`)
        var offset = 0
        while (offset < totalCount) {
            //console.log(`Runnning Batch from ${offset} to ${offset + limit}`)
            const results = await this.returnOffset(offset, limit)
            //console.log(`Got Result Count : ${results.length}`)
            await this.esClient.putContestsVotes(results)
            offset += limit
        }

        
    }

    // Main process
    async startExport(){
        console.log('Start Contest & Votes ingestion')
        try{
            await this.connect()
            const totalCount = await this.getTotalCount()
            console.log('Total Count here: ' + totalCount)
            const results = await this.batchProcess(totalCount)
            console.log('Contest & Votes Ingestion Completed')
            //await this.close()
            return true
        }catch(err){
            console.log('Contest & Votes export error: '+ err)
        }
    }
    
}

module.exports = ExportContestVotes